from dash import Dash, dcc, html
import plotly.express as px
import pandas 

app = Dash(__name__)

colors = {
    'background': '#111111',
    'text': '#7FDBFF'
}

# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options
df = pandas.read_excel('data/ghg-emission-of-thailand-edited-july-2021.xlsx')
df.columns = ['BE', 'emission_type', 'category', 'sub_category', 'en_sub_category', 'quantity']
power = df[df['emission_type'].str.contains('ภาคพลังงาน')]

fig = px.bar(df, x="BE", y="quantity", color="sub_category")

fig.update_layout(
    plot_bgcolor=colors['background'],
    paper_bgcolor=colors['background'],
    font_color=colors['text']
)

app.layout = html.Div(style={'backgroundColor': colors['background']}, children=[
    html.H1(
        children='Hello Dash',
        style={
            'textAlign': 'center',
            'color': colors['text']
        }
    ),

    html.Div(children='Dash: A web application framework for your data.', style={
        'textAlign': 'center',
        'color': colors['text']
    }),

    dcc.Graph(
        id='example-graph-2',
        figure=fig
    )
])

if __name__ == '__main__':
    app.run_server(debug=True)