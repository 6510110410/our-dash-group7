from dash import Dash, dcc, html
import dash_bootstrap_components as dbc
import plotly.express as px
import pandas 

app = Dash(__name__,external_stylesheets=[dbc.themes.VAPOR])

colors = {
    'background': '#111111',
    'text': '#aaff00'
}

df = pandas.read_excel("data_phuri/Passenger65.xlsx")
df.columns = ['Month','Year', 'project', 'total_passenger', 'passenger_per_day', 'normal day', 'day off']

pro_1 = df[df["project"].str.contains("โครงการรถไฟฟ้ามหานคร สายเฉลิมรัชมงคล")]
pro_2 = df[df["project"].str.contains("โครงการรถไฟฟ้ามหานคร สายฉลองรัชธรรม")]
pro_year = pro_1[pro_1["Year"] == 2557]
pro_year2 = pro_2[pro_2["Year"] == 2559]
test = px.bar(pro_1, x="Year", y="total_passenger", color="Month", orientation="v", barmode="group")
test1 = px.pie(pro_year,values="passenger_per_day",names="Month",color="Month",title="people in normal day")

test2 = px.bar(pro_2, x="Year", y="total_passenger", color="Month", orientation="v", barmode="group")
test3 = px.pie(pro_year2,values="passenger_per_day",names="Month",color="Month",title="people in normal day")


app.layout = html.Div(
    children=[
        html.Div(
            children=[
                html.H1(
                    children="Emission Of Thailand Edited July 2021",
                    style={
                        "textAlign": "center",
                    },
                )
            ],
            className="row",
        ),
        html.Div(
            children=[
                html.Div(
                    children="Dash: My Years are each other",
                    style={
                        "textAlign": "center",
                    },
                ),
            ],
            className="row",
        ),
        html.Div(
            children=[
                html.Div(
                    [dcc.Graph(id="example-graph-1", figure=test)], className="col"
                ),
                html.Div("\n"
                ),
                html.Div(
                    [dcc.Graph(id="example-graph-2", figure=test1)], className="col"
                ),
                html.Div("\n"
                ),
                html.Div(
                    [dcc.Graph(id="example-graph-3", figure=test2)], className="col"
                ),
                html.Div(
                    [dcc.Graph(id="example-graph-4", figure=test3)], className="col"
                ),
            ],
            className="row",
        ),
        html.Div([
            html.Button("Finish", className="btn btn-success")
        ],className="row",
        ),
    ]
)

if __name__ == "__main__":
    app.run_server(debug=True)