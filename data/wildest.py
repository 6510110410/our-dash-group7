from dash import Dash, dcc, html
import dash_bootstrap_components as dbc
import plotly.express as px
import pandas 

app = Dash(__name__, external_stylesheets=[dbc.themes.VAPOR])

colors = {"background": "#dc3545", "text": "#198754"}

# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options
df = pandas.read_excel("data/ghg-emission-of-thailand-edited-july-2021.xlsx")
df.columns = [
    "BE",
    "emission_type",
    "category",
    "sub_category",
    "en_sub_category",
    "quantity",
]
power = df[df["emission_type"].str.contains("ภาคของเสีย")]
# power_1 = power[power["BE"]==2543]
# power["BE"] = "2543"

power_2 = power.groupby(['BE']).min()[["quantity"]]
# power_2['BE'] = power_2.index
fig = px.line(power, x='BE', y='quantity', color="category",title="Forest Sector and Land Useful")
fig1 = px.bar(power, x="BE", y="quantity", color="category",orientation="v",barmode="overlay",title="Forest Sector and Land Useful")
fig3 = px.pie(power_2,values="quantity",names =  "quantity",template="plotly_white",title="Forest Sector and Land Useful")


app.layout = html.Div(
    children=[
        html.Div(
            children=[
                html.H1(
                    children="Emission Of Thailand Edited July 2021",
                    style={
                        "textAlign": "center",
                    },
                )
            ],
            className="row",
        ),
        html.Div(
            children=[
                html.Div(
                    children="Dash: My Years are each other",
                    style={
                        "textAlign": "center",
                    },
                ),
            ],
            className="row",
        ),
        html.Div(
            children=[
                html.Div(
                    [dcc.Graph(id="example-graph-1", figure=fig)], className="col"
                ),
                html.Div("\n"
                ),
                html.Div(
                    [dcc.Graph(id="example-graph-2", figure=fig1)], className="col"
                ),
                html.Div("\n"
                ),
                html.Div(
                    [dcc.Graph(id="example-graph-3", figure=fig3)], className="col"
                ),
            ],
            className="row",
        ),
        html.Div([
            html.Button("Finish", className="btn btn-success")
        ],className="row",
        ),
    ]
)

if __name__ == "__main__":
    app.run_server(debug=True)
