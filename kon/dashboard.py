from dash import Dash,html,dcc
import pandas
import plotly.express as px
import dash_bootstrap_components as dbc

app = Dash(__name__, external_stylesheets=[dbc.themes.SUPERHERO])

df = pandas.read_excel("data/data.xlsx")

figall = px.bar(df, x=df['ปี'], y=df['จำนวนผู้โดยสารรวม'],color=df["เดือน"], title="ปี 2557 - 2565")

year57 = df[df["ปี"] == 2557]
fig57 = px.bar(year57, x=year57['เดือน'], y=year57['จำนวนผู้โดยสารรวม'],color=year57['จำนวนผู้โดยสารรวม'], title="ปี 2557")

year58 = df[df["ปี"] == 2558]
fig58 = px.bar(year58, x=year58['เดือน'], y=year58['จำนวนผู้โดยสารรวม'],color=year58['จำนวนผู้โดยสารรวม'], title="ปี 2558")

year59 = df[df["ปี"] == 2559]
fig59 = px.bar(year59, x=year59['เดือน'], y=year59['จำนวนผู้โดยสารรวม'],color=year59['จำนวนผู้โดยสารรวม'], title="ปี 2559")

year60 = df[df["ปี"] == 2560]
fig60 = px.bar(year60, x=year60['เดือน'], y=year60['จำนวนผู้โดยสารรวม'],color=year60['จำนวนผู้โดยสารรวม'], title="ปี 2560")

year61 = df[df["ปี"] == 2561]
fig61 = px.bar(year61, x=year61['เดือน'], y=year61['จำนวนผู้โดยสารรวม'],color=year61['จำนวนผู้โดยสารรวม'], title="ปี 2561")

year62 = df[df["ปี"] == 2562]
fig62 = px.bar(year62, x=year62['เดือน'], y=year62['จำนวนผู้โดยสารรวม'],color=year62['จำนวนผู้โดยสารรวม'], title="ปี 2562")

year63 = df[df["ปี"] == 2563]
fig63 = px.bar(year63, x=year63['เดือน'], y=year63['จำนวนผู้โดยสารรวม'],color=year63['จำนวนผู้โดยสารรวม'], title="ปี 2563")

year64 = df[df["ปี"] == 2564]
fig64 = px.bar(year64, x=year64['เดือน'], y=year64['จำนวนผู้โดยสารรวม'],color=year64['จำนวนผู้โดยสารรวม'], title="ปี 2564")

year65 = df[df["ปี"] == 2565]
fig65 = px.bar(year65, x=year65['เดือน'], y=year65['จำนวนผู้โดยสารรวม'],color=year65['จำนวนผู้โดยสารรวม'], title="ปี 2565")

app.layout = html.Div(
    children=[
        html.Div(
            children=[
                html.H1(
                    children="โครงการรถไฟฟ้ามหานคร สายเฉลิมรัชมงคล",
                    style={"textAlign": "center",},
                ),
                html.P(
                    children=""
                ),
            ]
        ),
        html.Div(
            children=[
                html.Div(
                    [dcc.Graph(figure=figall)]
                ),
                html.P(
                    children=""
                ),
            ]
        ),
        html.Div(
            children=[
                html.Div(
                    [dcc.Graph(id="example-graph-1", figure=fig57)], className="col"
                ),
                html.Div(
                    [dcc.Graph(id="example-graph-2", figure=fig58)], className="col"
                ),
                html.P(
                    children=""
                ),
            ],
            className="row"         
        ),
        html.Div(
            children=[
                html.Div(
                    [dcc.Graph(id="example-graph-3", figure=fig59)], className="col"
                ),
                html.Div(
                    [dcc.Graph(id="example-graph-4", figure=fig60)], className="col"
                ),
                html.P(
                    children=""
                ),
            ],
            className="row"         
        ),
        html.Div(
            children=[
                html.Div(
                    [dcc.Graph(id="example-graph-5", figure=fig61)], className="col"
                ),
                html.Div(
                    [dcc.Graph(id="example-graph-6", figure=fig62)], className="col"
                ),
                html.P(
                    children=""
                ),
            ],
            className="row"         
        ),
        html.Div(
            children=[
                html.Div(
                    [dcc.Graph(id="example-graph-7", figure=fig63)], className="col"
                ),
                html.Div(
                    [dcc.Graph(id="example-graph-8", figure=fig64)], className="col"
                ),
                html.P(
                    children=""
                ),
            ],
            className="row"         
        ),
        html.Div(
            children=[
                html.Div(
                    [dcc.Graph(id="example-graph-9", figure=fig65)], className="col"
                ),
            ],
            className="row"         
        ),
    ]
)

if __name__ == "__main__":
    app.run_server(debug=True)