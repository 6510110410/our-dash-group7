from dash import Dash, dcc, html, Input, Output
import dash_bootstrap_components as dbc
import plotly.express as px
import pandas

app = Dash(__name__, external_stylesheets=[dbc.themes.VAPOR])

colors = {"background": "#111111", "text": "#7FDBFF"}

# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options
df = pandas.read_excel("data1/Passenger65.xlsx")
df.columns = [
    "MONTH",
    "BE",
    "Project",
    "sum_passenger",
    "usual_date",
    "quantity",
    "weekend"
]
df["sum_passenger"] = df["sum_passenger"].replace("-", 0)
power = df[df["Project"].str.contains("โครงการรถไฟฟ้ามหานคร สายฉลองรัชธรรม")]
power_2 = power.groupby(['BE']).max()[["sum_passenger"]]
figall = px.bar(df, x=df['BE'], y=df['sum_passenger'],color=df["MONTH"], title="ปี 2557 - 2565")
pro_2 = df[df["Project"].str.contains("โครงการรถไฟฟ้ามหานคร สายฉลองรัชธรรม")]
test2 = px.bar(pro_2, x="BE", y="sum_passenger", color="MONTH", orientation="v", barmode="group",title="Project your Passenger")
fig2 = px.scatter(power_2, x=df['BE'], y=df['weekend'], color=df["MONTH"],title="Electric Train Project")
@app.callback(
    Output("pie-graph", "figure"),
    Input("year", "value"),
    Input("Project", "value"),
)
def show_data(selected_month, category):
    print(selected_month, category)
    filtered_df = df[df["BE"] == int(selected_month)][
        df["Project"].str.contains(category)
    ]
    # print(filtered_df)
    fig = px.pie(filtered_df,values="sum_passenger", names="MONTH",title="Electric Train Project")
        # color="sub_category",
    
    # # fig.update_layout(transition_duration=500)
    return fig 

app.layout = html.Div(
    children= [
        html.Div(
            children=[
                html.H1(
                    children="Hello Dash",
                    style={
                        "textAlign": "center",
                    },
                )
            ],
            className="row",
        ),
        html.Div(
            children=[
                html.Div(
                    children="Dash: number of train passengers.",
                    style={
                        "textAlign": "center",
                    },
                ),
            ],
            className="row",
        ),
        html.Div(
            children=[
                html.Div(
                    [dcc.Graph(figure=figall)]
                ),
                html.P(
                    children=""
                ),
            ]
        ),

        html.Div(
            children=[
                html.Div(
                    [dcc.Graph(id="example-graph-2", figure=fig2)], className="col"
                ),
                html.Div(
                    [dcc.Graph(id="example-graph-3", figure=test2)], className="col"
                ),

        html.Div(
           "\n"
        ),

        html.Div(
            [
                html.Div(
                    [
                        dcc.Graph(id="pie-graph"),
                    ],
                    className="col-8",
                ),
                
            ],
            ),

        html.Div(
           "\n"
        ),
        
                html.Div(
                    [
                        # dbc.Dropdown(df["BE"].unique(), df["BE"].min(), id="year"),
                        # dbc.Dropdown(
                        #     df["emission_type"].unique(),
                        #     df["emission_type"][0],
                        #     id="emission_type",
                        # ),
                        dbc.Select(
                            options=[
                                dict(label=be, value=be) for be in df["BE"].unique()
                            ],
                            id="year",
                            value=df["BE"].min(),
                        ),
                        dbc.Select(
                            options=[
                                dict(label=et.strip(), value=et.strip())
                                for et in df["Project"].unique()
                            ],
                            id="Project",
                            value=df["Project"][0],
                        ),
                    ],
                    className="col-4",
                ),
            ],
            className="row",
        ),
    ],
    className="container-fluid",
)

if __name__ == "__main__":
    app.run_server(debug=True)
