from dash import Dash, dcc, html
import dash_bootstrap_components as dbc
import plotly.express as px
import pandas 

app = Dash(__name__, external_stylesheets=[dbc.themes.LUX])

colors = {"background": "#dc3545", "text": "#198754"}

# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options
df = pandas.read_excel("data/ghg-emission-of-thailand-edited-july-2021.xlsx")
df.columns = [
    "BE",
    "emission_type",
    "category",
    "sub_category",
    "en_sub_category",
    "quantity",
]
df["emission_type"] =df["emission_type"].replace("quantity",0)
power = df[df["emission_type"].str.contains("ภาคป่าไม้")]
# power_1 = power[power["BE"]==2543]
# power["BE"] = "2543"
# power_2 = pandas.concat([power],axis = 0)
# power_2 = power_2.groupby(['BE']).sum()[["quantity"]]
# power_2['BE'] = power_2.index

fig = px.line(power, x='BE', y='quantity', color="sub_category")

fig2 = px.bar(power, x="BE", y="quantity", color="sub_category",orientation="v",barmode="stack")

fig3 = px.pie(power,names = "quantity",color="category")


app.layout = html.Div(
    children=[
        html.Div(
            children=[
                html.H1(
                    children="Hello Dash",
                    style={
                        "textAlign": "center",
                    },
                )
            ],
            className="row",
        ),
        html.Div(
            children=[
                html.Div(
                    children="Dash: A web application framework for your data.",
                    style={
                        "textAlign": "center",
                    },
                ),
            ],
            className="row",
        ),
        html.Div(
            children=[
                html.Div(
                    [dcc.Graph(id="example-graph-1", figure=fig)], className="col"
                ),
                html.Div(
                    [dcc.Graph(id="example-graph-2", figure=fig2)], className="col"
                ),
                html.Div(
                    [dcc.Graph(id="example-graph-2", figure=fig3)], className="col"
                ),
            ],
            className="row",
        ),
        html.Div([
            html.Button("Hello", className="btn btn-primary")
        ],className="row",
        ),
    ]
)

if __name__ == "__main__":
    app.run_server(debug=True)
